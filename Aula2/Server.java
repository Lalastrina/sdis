import java.io.IOException;

public class Server {

	//java Server <port_number> <m_address> <m_port>
	public static void main(String[] args) throws NumberFormatException, IOException {
		
		//multicast
		new ServerThreadMulti(new Integer(args[0]), args[1], new Integer(args[2])).start();
		//normal socket
		new ServerThread(new Integer(args[0])).start();
	}

}
