import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;

public class ServerThreadMulti extends Thread {

	private String multiAddress;
	private int multiPort;
	private int port;
	
	private MulticastSocket multiSocket;
	private boolean hasRequest;
	
	public ServerThreadMulti(int port, String multiAddress, int multiPort) throws IOException {
		this.multiAddress = multiAddress;
		this.multiPort = multiPort;
		this.port = port;		
		hasRequest = false;
	}
	
	public void run() {
		
		try {
			
			System.out.println("Server multicast running...");
			
			multiSocket = new MulticastSocket(multiPort);
			
			//broadcast its address (multicast address + multicast port)
			String messageAddress = InetAddress.getLocalHost().getHostName() + "," + port;
			byte[] messageB = messageAddress.getBytes();
			InetAddress address = InetAddress.getByName(multiAddress);
			DatagramPacket dataPacket = new DatagramPacket(messageB, messageB.length, address, multiPort);
			int ttl = multiSocket.getTimeToLive(); 
			int newttl = 1;
			
			while(!hasRequest) {				
				multiSocket.setTimeToLive(newttl); 
				multiSocket.send(dataPacket); 
				multiSocket.setTimeToLive(ttl);
				//multicast: <mcast_addr> <mcast_port>: <srvc_addr> <srvc_port> 
				System.out.println("multicast: " + multiAddress + " " + multiPort + ": " + messageAddress);
						
				Thread.sleep(1000);
			}
			
		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
            System.out.println("Error in thread 1.");
        } finally {
        	multiSocket.close();
        }	
	}

}