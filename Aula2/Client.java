import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.MulticastSocket;

public class Client {

	private int port;
	private String address;
	private String operation;
	private String plateNumber;
	private String owner;
	
	public Client(String[] args){
		port = new Integer(args[1]);
		address = args[0];
		operation = args[2].toUpperCase();
		plateNumber = args[3];
		owner = "";
		
		if(operation.equals("REGISTER")){
			if(args.length != 5){
				System.out.println("java Client <mcast_addr> <mcast_port> <oper> <opnd> ");
				return;
			}
			
			owner = args[4];			
		}
	}
	
	public void run() throws IOException {
		
		try {
			System.out.println("Client running...");
			
			//join group and get port/address to send request
			InetAddress iaddress = InetAddress.getByName(address);
			MulticastSocket multiSocket = new MulticastSocket(port);
			multiSocket.joinGroup(iaddress);
			
			byte[] buf = new byte[256];
			DatagramPacket serviceInfo = new DatagramPacket(buf, buf.length);
			multiSocket.receive(serviceInfo);		
			String msg = new String(serviceInfo.getData(), 0, serviceInfo.getLength());
			String[] info = msg.split(",");		
			int portToSend = new Integer(info[1]);
			
			System.out.println("multicast: " + address + " " + port + ": " + info[0] + " " + portToSend);
			//create message
			String message = operation + " " + plateNumber + (owner.isEmpty()? "" : " " + owner);
			System.out.println("Message to send: " + message);
			
			//open socket
			DatagramSocket socket = new DatagramSocket();
			
			//send message
			byte[] messageB = message.getBytes();
			InetAddress addressToSend = InetAddress.getByName(info[0]);
			DatagramPacket packetToSend = new DatagramPacket(messageB, messageB.length, addressToSend, portToSend);
			socket.send(packetToSend);
			
			//wait response
			System.out.println("Waiting response...");
			byte[] buffer = new byte[256];
			DatagramPacket packetToReceive = new DatagramPacket(buffer, buffer.length);
			socket.receive(packetToReceive);
			
			System.out.println(message + " : " + new String(packetToReceive.getData(), 0, packetToReceive.getLength()));	
			
			//close socket
			multiSocket.close();
			socket.close();	
			
		} catch (IOException e) {
			e.printStackTrace();
		} finally {			
			System.out.println("Ending...");
		}
		
	}
	
	//java Client <mcast_addr> <mcast_port> <oper> <opnd>*
	public static void main(String[] args) throws IOException {
		
		if(args.length < 4){
			System.out.println("java Client <mcast_addr> <mcast_port> <oper> <opnd> ");
			return;
		}
		
		Client c = new Client(args);
		c.run();		
		
	}

	public int getPort() {
		return port;
	}

	public String getAddress() {
		return address;
	}

	public String getOperation() {
		return operation;
	}

	public String getPlateNumber() {
		return plateNumber;
	}

	public String getOwner() {
		return owner;
	}

	
}