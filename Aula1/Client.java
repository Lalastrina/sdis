import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class Client {

	private int port;
	private String host;
	private String operation;
	private String plateNumber;
	private String owner;
	
	public Client(String[] args){
		port = new Integer(args[1]);
		host = args[0];
		operation = args[2].toUpperCase();
		plateNumber = args[3];
		owner = "";
		
		if(operation.equals("REGISTER")){
			if(args.length != 5){
				System.out.println("java Client <host_name> <port_number> <oper> <opnd>");
				return;
			}
			
			owner = args[4];			
		}
	}
	
	//java Client <host_name> <port_number> <oper> <opnd>*
	public static void main(String[] args) throws IOException {
		
		if(args.length < 4){
			System.out.println("java Client <host_name> <port_number> <oper> <opnd>");
			return;
		}
		
		Client c = new Client(args);
		
		//create message
		String message = c.getOperation() + " " + c.getPlateNumber() + (c.getOwner().isEmpty()? "" : " " + c.getOwner());
		System.out.println("Message to send: " + message);
		
		//open socket
		DatagramSocket socket = new DatagramSocket();
		
		//send message
		byte[] messageB = message.getBytes();
		//InetAddress address = InetAddress.getByName(c.getHost());
		InetAddress address = InetAddress.getByName(InetAddress.getLocalHost().getHostName());
		//System.out.println(InetAddress.getLocalHost().getHostName());
		DatagramPacket packetToSend = new DatagramPacket(messageB, messageB.length, address, c.getPort());
		socket.send(packetToSend);
		
		//wait response
		System.out.println("Waiting response...");
		byte[] buffer = new byte[256];
		DatagramPacket packetToReceive = new DatagramPacket(buffer, buffer.length);
		socket.receive(packetToReceive);
		
		System.out.println(message + " : " + new String(packetToReceive.getData(), 0, packetToReceive.getLength()));	
				
		//close socket
		socket.close();	
		
		System.out.println("Ending...");
		
	}

	public int getPort() {
		return port;
	}

	public String getHost() {
		return host;
	}

	public String getOperation() {
		return operation;
	}

	public String getPlateNumber() {
		return plateNumber;
	}

	public String getOwner() {
		return owner;
	}

	
}
