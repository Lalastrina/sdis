import java.io.IOException;

public class Server {

	//java Server <port_number>
	public static void main(String[] args) throws NumberFormatException, IOException {
		
		new ServerThread(new Integer(args[0])).start();
		
	}

}
