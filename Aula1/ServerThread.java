import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;

public class ServerThread extends Thread {

	private int port;
	private DatagramSocket socket;
	private boolean isWaiting;
	
	public ServerThread(int port) throws SocketException {
		super("Server09");
		this.port = port;
		socket = new DatagramSocket(port);
		isWaiting = true;
	}
	
	public void run() {
		
		try {
			System.out.println("Server running...");
			
			//waiting for request
			while(isWaiting) {
				
				byte[] buf = new byte[256];
				byte[] reply;
				DatagramPacket packet = new DatagramPacket(buf, buf.length);
				
				//receive request
				
				socket.receive(packet);
				
				
				//interpret message				
				String message = new String(packet.getData(), 0, packet.getLength());
				String[] data = message.split(" ");
				
				System.out.println("Received request: " + message);
				
				//build reply
				if(data[0].toUpperCase().equals("REGISTER")) {
					int result = register(data[1], data[2]);
					reply = Integer.toString(result).getBytes();
				}
				else {
					String result = lookUp(data[1]);
					reply = result.getBytes();
				}
				
				//send reply
				InetAddress addressClient = packet.getAddress();
				int portClient = packet.getPort();
				DatagramPacket packetSend = new DatagramPacket(reply, reply.length, addressClient, portClient);
				socket.send(packetSend);			
				System.out.print("Sent reply: " + new String(packetSend.getData(), 0, packetSend.getLength()));
				isWaiting = false;
					
			}
			
			System.out.println("Closing...");
			socket.close();
		
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public int register(String plateNumber, String owner) throws IOException {
		PrintWriter out = null;
		BufferedReader in = null;
		
		int lines = -1;
		try {
			System.out.println("WRITEEE");
		    out = new PrintWriter(new BufferedWriter(new FileWriter("db.txt", true)));
		    out.println(plateNumber);
		    out.println(owner);
		    
		} catch (IOException e) {
			e.printStackTrace();
		    System.out.println("Error occured while trying to write to DB.");
		} finally {
			out.close();
		}
		
		try {
			in = new BufferedReader(new FileReader("db.txt"));
			lines = 0;
			String s;
			while((s = in.readLine()) != null) {
				System.out.println(s);
				lines++;
			}
			
			lines /= 2;
				
		} catch (FileNotFoundException e) {
			e.printStackTrace();
            System.out.println("Could not open db file");
        } finally {
        	in.close();
        }	

		return lines;		
	}

	public String lookUp(String plateNumber) throws IOException {
		String result = "NOT_FOUND";
		BufferedReader in = null;
		boolean found = false;
		
		try {
			
            in = new BufferedReader(new FileReader("db.txt"));
            
            while(!found) {
    			
    			String line1 = in.readLine();//plate number
    			
    			if(line1 == null) 
    				break;
    			
    			String line2 = in.readLine();//owner
    			
    			if(plateNumber.equals(line1)) {
    				result = line2;
    				found = true;	
    			}
    		}
            
        } catch (FileNotFoundException e) {
        	e.printStackTrace();
            System.out.println("Could not open db file");
        } finally {
        	in.close();
        }		
		
		return result;
		
	}

}
