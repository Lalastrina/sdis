var Types = require('hapi').types;
var Joi = require('Joi');

/* Routes */
module.exports = [{
    method: 'GET',
    path: '/products',
    config: {
        handler: getProducts,
        validate: {
            query: {
                name: Types.String()
            }
        }
    }
}, {
    method: 'GET',
    path: '/products/{id}',
    config: {
        handler: getProduct
    }
}, {
    method: 'POST',
    path: '/products',
    config: {
        handler: addProduct,
        payload: 'parse',
        validate: {
            payload: {
                name: Types.String().required().min(3)
            }
        }
    }
}, {
    method: 'GET',
    path: '/places',
    config: {
        handler: getPlaces,
    }
}, {
    method: 'GET',
    path: '/places/{id}',
    config: {
        handler: getPlace
    }
}];

var products = [{
        id: 1,
        name: 'Guitar'
    },
    {
        id: 2,
        name: 'Banjo'
    }
];

/* DB */
var sqlite3 = require('sqlite3').verbose();
var db = new sqlite3.Database('memory.db');

/**
* columns - columns to select
* table - table to select from
* whereStatement - where statement for the query
* request - the request variable if it exists in the parent function
* callbackFUnction - the name of the function to run after the query finishes
*/
function selectRowsFromDatabase(columns, table, whereStatement, request, callbackFunction) {

    db.serialize(function() {
        var query = "";

        if(whereStatement.length != 0)
            query = "SELECT " + columns + " FROM " + table + " WHERE " + whereStatement;
        else
            query = "SELECT " + columns + " FROM " + table;

        db.all(query, function(err, rows) {
            callbackFunction(rows, request);
        });
    });

}

/* Callback functions */
function replyInJSon(rows, request) {
    console.log(rows);
    request.reply(rows);
}


/* Controllers */

//get one place
function getPlace(request) {
    if (!isNaN(request.params.id)) {
        selectRowsFromDatabase("id,placeID,rating", "lugares", "id = "+request.params.id, request, replyInJSon);
    }
    else {
        request.reply(null);
    }    
}

//get all places
function getPlaces(request) {
    selectRowsFromDatabase("*", "lugares", "", request, replyInJSon);
}

//get user
function getUser(request) {
    if (!isNaN(request.params.id)) {
        selectRowsFromDatabase("id,placeID,rating", "lugares", "id = "+request.params.id, request, replyInJSon);
    }
    else {
        request.reply(null);
    }    
}

function getProducts(request) {

    if (request.query.name) {
        request.reply(findProducts(request.query.name));
    }
    else {
        request.reply(products);
    }
}

function findProducts(name) {

    return products.filter(function(product) {
        return product.name.toLowerCase() === name.toLowerCase();
    });
}

function getProduct(request) {

    var product = products.filter(function(p) {
        return p.id === parseInt(request.params.id);
    }).pop();

    request.reply(product);
}

function addProduct(request) {

    var product = {
        id: products[products.length - 1].id + 1,
        name: request.payload.name
    };

    products.push(product);

    request.reply(product).code(201).header('Location', '/products/' + product.id);
}