angular.module('starter.controllers', [])

.controller('HomeCtrl', function($scope) {})

.controller('LoginCtrl', function($scope, $state) {
  
  	//login button
	$scope.login = function(data) {

		$state.go('conta', {}, {reload: true});	
	} 

	//sign up button
	$scope.registar = function() {

		$state.go('registar', {}, {reload: true});	
	}  
})

.controller('RegistoCtrl', function($scope, $state) {
  
	//sign up button
	$scope.registar = function(data) {

		$state.go('conta', {}, {reload: true});	
	}  
})

.controller('GoBackCtrl', function($scope, $ionicHistory) {
  
	$scope.goBack = function() {
		 $ionicHistory.goBack();
	} 

});