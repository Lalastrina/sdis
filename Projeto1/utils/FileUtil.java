package utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import main.InitiatorPeer;


public class FileUtil {

	private File file;
	private String filename;
	private int replicationDegree;
	private String fileID;
	private byte[] fileHash;
	private long fileSize;
	private long lastModified;
	private int numChunks;
	
	public FileUtil(String filename, int repDeg) {
		
		this.filename = filename;	
		this.replicationDegree = repDeg;
		if(repDeg != -1) file = new File(filename);
		
	}
	
	/*
	 * Checks if file exists and extracts information from it
	 * (like last modification date and size).
	 * 
	 */
	public boolean treatFile() {
				
		if(!file.isFile()){
			System.out.println("The file provided by that path is not a normal file. Perhaps it's hidden or a directory?");
			return false;
		}
		
		lastModified = file.lastModified();
		fileSize = file.length();		
		//8 + 3 + 64 + 6 + 1
		numChunks = (int) fileSize/Chunk.MAX_SIZE  + 1;
		//System.out.println("FILE :" + fileSize);
		if(numChunks > 1000000) {
			System.out.println("File too big.");
			return false;
		}
		
		return true;
	}
	
	/*
	 * Generates encrypted id for the file, using the following file metadata for  
	 * the sha256 hash: filename, filesize and date from last modification.
	 * 
	 */
	public void generateEncryptedFileID() {
		
		//generate sha256 hash
		MessageDigest sha;
		
		try {
			
			sha = MessageDigest.getInstance("SHA-256");
			String toDigest = filename + " " + fileSize + " " + lastModified;
			sha.update(toDigest.getBytes("UTF-8"));
			fileHash = sha.digest();
			
		} catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
			System.out.println("SHA256 error.");
			e.printStackTrace();
		}
		
		//byte to hex taken from stackoverflow -> http://stackoverflow.com/questions/2817752/java-code-to-convert-byte-to-hexadecimal
	    StringBuilder sb = new StringBuilder();
	    for (byte b : fileHash) {
	        sb.append(String.format("%02X", b));
	    }
	    
	   fileID = sb.toString();
		
	}

	
	/*
	 * Splits the file into chunks.
	 * 
	 * @return An array of chunks
	 */
	public Chunk[] getChunks() {
		
		Chunk[] chunks = new Chunk[numChunks];
		int totalRead = 0;		
		int portionSize = 0;	
		int currentSize = 0;
		FileInputStream br = null;
		byte[] data;
		
		try {
			
			br = new FileInputStream(filename);
			
			for(int i = 0; i < numChunks; i++) {
				
				if(i == numChunks - 1) {
					currentSize = (int) (fileSize - totalRead);
				}
				else {
					currentSize = Chunk.MAX_SIZE;					
				}
				
				if(currentSize != 0) {
					data = new byte[currentSize];
					portionSize = br.read(data);
					Chunk chunk = new Chunk(i, fileID, replicationDegree, data);
					
					chunks[i] = chunk;
					totalRead += portionSize;
				}
				else {
					Chunk chunk = new Chunk(i, fileID, replicationDegree, null);	
					chunks[i] = chunk;
				}
			}
			
			br.close();
			
		} catch (IOException e1) {			
			System.out.println("Error while splitting file into chunks.");
			e1.printStackTrace();
		}

		return chunks;
	}
	
	public static boolean saveChunk(Chunk chunk, String chunkDir) {

		File dir = new File(chunkDir);
		
		if(!dir.exists()) {
			dir.mkdir();
		}
		
		String filename = chunk.getFileID() + "." + chunk.getId(); 
		
		File fileChunk = new File(chunkDir+"/"+filename);
		
		if(fileChunk.exists())
			return false;
		
		if(chunk.getDataB().length > 0) {
			try {
				FileOutputStream writer = new FileOutputStream(fileChunk);
				writer.write(chunk.getDataB());
				writer.close();
			} catch (IOException e) {
				e.printStackTrace();
				return false;
			}
			
		}
		
		return true;
	}
	
	public static void addChunkToFile(String filename, String data) {
		
		File dir = new File(InitiatorPeer.logFile.getRestore_dir());
		
		if(!dir.exists()) 
			dir.mkdir();
		
		FileWriter fw = null;
		
		try {
			fw = new FileWriter(InitiatorPeer.logFile.getRestore_dir()+"/"+filename,true);
			fw.write(data);
			fw.close();
		} catch(IOException e) {
			
			if(fw != null) {
				try {
					fw.close();
				} catch(IOException e1) {
					e.printStackTrace();
				}
			}
		}		
		
	}
	
	public static void addChunkToFile(String filename, byte[] data) {
		
		File dir = new File(InitiatorPeer.logFile.getRestore_dir());
		
		if(!dir.exists()) 
			dir.mkdir();
		
		FileOutputStream output = null;
		try {
			output = new FileOutputStream(InitiatorPeer.logFile.getRestore_dir()+"/"+filename, true);
			output.write(data);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			
		   if(output != null) {
			   try {
				output.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		   }
		   
		}
		
	}
	
	public static byte[] getChunkData(String fileHash, String id, String chunkDirPath) {
		
		File dir = new File(chunkDirPath);
		
		if(!dir.exists()) 
			dir.mkdir();
		
		String filename = chunkDirPath+"/" + fileHash + "." + id; 
		
		File fileChunk = new File(filename);
		byte[] data = null;
		
		if(fileChunk.exists()) {
			try {
				data = Files.readAllBytes(Paths.get(filename));
			} catch (IOException e) {
				return null;
			}
		}
		else
			return null;
		
		return data;

	}
	
	public static byte[] getChunkData(String fileHash, int id, String chunkDirPath) {
		
		File dir = new File(chunkDirPath);
		
		if(!dir.exists()) 
			dir.mkdir();
		
		String filename = chunkDirPath+"/" + fileHash + "." + id; 
		
		File fileChunk = new File(filename);
		byte[] data = null;
		
		if(fileChunk.exists()) {
			try {
				data = Files.readAllBytes(Paths.get(filename));
			} catch (IOException e) {
				return null;
			}
		}
		else
			return null;
		
		return data;

	}
	
	public static boolean deleteChunk(String chunkName, String chunkDir) {
		
		Path p1 = Paths.get(chunkDir+"/"+chunkName);
		
		try {
			Files.deleteIfExists(p1);
		} catch (IOException e) {
			return false;
		}
		
		return true;
	}
	
	/**
	 * @return the encrypted file id
	 */
	public String getFileID() {
		return fileID;
	}

	
}
