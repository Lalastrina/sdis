package utils;

public class PeerMessage {
	
	private String[] message;
	private String origin;
	private byte[] data;
	
	public PeerMessage(String[] message, String origin) {
		this.setMessage(message);
		this.setOrigin(origin);
	}

	/**
	 * @return the message
	 */
	public String[] getMessage() {
		return message;
	}

	/**
	 * @param message the message to set
	 */
	public void setMessage(String[] message) {
		this.message = message;
	}

	/**
	 * @return the origin
	 */
	public String getOrigin() {
		return origin;
	}

	/**
	 * @param origin the origin to set
	 */
	public void setOrigin(String origin) {
		this.origin = origin;
	}

	/**
	 * @return the data
	 */
	public byte[] getData() {
		return data;
	}

	/**
	 * @param data the data to set
	 */
	public void setData(byte[] data) {
		this.data = data;
	}
}
