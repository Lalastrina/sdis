package utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import main.InitiatorPeer;

public class Log {

	private String log_dir = "log";
	private String restore_dir = "restore";
	private String backedup_name = "backedup.txt";
	private String chunks_name = "chunks1.txt";
	private String log_name = "log.txt";	

	
	private HashMap<String,String> backedupFiles; //files that were backed up through here
	private ArrayList<Chunk> chunksBeingSaved; //chunks being saved here
	
	public Log() {
		setBackedupFiles(new HashMap<String,String>());
		setChunksBeingSaved(new ArrayList<Chunk>());
		
		File dir = new File(log_dir);
		if(!dir.exists()) 
			dir.mkdir();
		
		File dir1 = new File(restore_dir);
		if(!dir1.exists()) 
			dir1.mkdir();
		
		load();
	}

	public void load() {
		
		File backedup = new File(log_dir + "/" + backedup_name);
		File chunkssaved = new File(log_dir + "/" + chunks_name);
		File log = new File(log_dir + "/" + log_name);
		
		if(backedup.exists()) 
		{
			Scanner fileIn = null;
			try {
				fileIn = new Scanner(backedup);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
			
			String line = "";

			while(fileIn.hasNextLine()) {	
				line = fileIn.nextLine();
				//filename,filehash numberChunks
				String[] parts = line.split(",");
				backedupFiles.put(parts[0], parts[1]);
				
			}
			
			fileIn.close();
		}
		
		if(chunkssaved.exists()) 
		{
			Scanner fileIn = null;
			
			try {
				fileIn = new Scanner(chunkssaved);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
			
			String line = "";
			
			while(fileIn.hasNextLine()) {
				line = fileIn.nextLine();
				String[] parts = line.split(",");
				String[] identity = parts[0].split(" ");
				String[] repDeg = parts[1].split(" ");
				int size = Integer.parseInt(parts[2]);
				Chunk chunk = new Chunk(Integer.parseInt(identity[1]), identity[0], Integer.parseInt(repDeg[0]), null);
				chunk.setLength(size);
				chunk.setActualReplicationDegree(Integer.parseInt(repDeg[1]));
				addChunkBeingSaved(chunk);
			}
			
			fileIn.close();
		}
		
		if(log.exists()) 
		{
			Scanner fileIn = null;
			try {
				fileIn = new Scanner(log);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
			
			int maxSpace = Integer.parseInt(fileIn.nextLine());
			int usedSpace = Integer.parseInt(fileIn.nextLine());
			InitiatorPeer.setMaxSpace(maxSpace, false);
			InitiatorPeer.setUsedSpace(usedSpace);
			
			fileIn.close();
		}
	
	}
	
	public synchronized void saveBackedUp() {
		
		File dir = new File(log_dir);
		
		if(!dir.exists()) {
			dir.mkdir();
		}		
		
		File file = new File(log_dir + "/" + backedup_name);
		
		try {
			FileOutputStream writer = new FileOutputStream(file);
			
	
				for (Map.Entry<String, String> entry : backedupFiles.entrySet()) {
				    String key = entry.getKey();
				   	String value = entry.getValue();
				    String line = key + "," + value + "\r\n";
				    writer.write(line.getBytes(StandardCharsets.US_ASCII));
				}	
				
				writer.close();
		} catch (IOException e) {

			e.printStackTrace();

		}

	}
	
	public synchronized void saveChunksBeingBackedUp() {//ArrayList<Chunk> chunks) {
		
		File dir = new File(log_dir);
		
		if(!dir.exists()) {
			dir.mkdir();
		}		
		
		File file = new File(log_dir + "/" + chunks_name);
		
		try {
			FileOutputStream writer = new FileOutputStream(file);
			
	
				for (Chunk chunk : getChunksBeingSaved()) {
					//fileHash chunkID,repDeg actualRepDeg,size
				    String line = chunk.getFileID() + " " + chunk.getId() + "," + chunk.getReplicationDegree() + " " + chunk.getActualReplicationDegree() + "," + chunk.getLength() + "\r\n";
				    writer.write(line.getBytes(StandardCharsets.US_ASCII));
				}	
				
				writer.close();
		} catch (IOException e) {
			e.printStackTrace();

		}

	}
	
	public synchronized void saveLog() {
		
		File dir = new File(log_dir);
		
		if(!dir.exists()) {
			dir.mkdir();
		}		
		
		File file = new File(log_dir + "/" + log_name);
		String v = InitiatorPeer.getMaxSpace() + "\r\n";
		String v1 = InitiatorPeer.getUsedSpace() + "\r\n";
		
		try {
			FileOutputStream writer = new FileOutputStream(file);
			writer.write(v.getBytes(StandardCharsets.US_ASCII));
			writer.write(v1.getBytes(StandardCharsets.US_ASCII));
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();

		}
		
	}
	
	public synchronized void addFileToBacked(String filename, String fileHash) {
		
		backedupFiles.put(filename, fileHash);
	}

	public synchronized boolean hasFileBacked(String filename) {
		
		if(backedupFiles.get(filename) != null)
			return true;
		
		return false;
	}
	
	public synchronized boolean hasFileBackedByFileHash(String fileHash) {
		
	    for(String key : backedupFiles.values()){
	    	String[] value = key.split(" ");
	        if(value[0].equals(fileHash))
	        	return true;
	    }
		
		return false;
	}
	
	public synchronized String getFileHash(String filename) {
		
		return backedupFiles.get(filename);
	}
	
	
	/**
	 * @return the backedupFiles
	 */
	public synchronized HashMap<String,String> getBackedupFiles() {
		return backedupFiles;
	}

	/**
	 * @param backedupFiles the backedupFiles to set
	 */
	public synchronized void setBackedupFiles(HashMap<String,String> backedupFiles) {
		this.backedupFiles = backedupFiles;
	}
	
	/**
	 * @param backedupFiles the backedupFiles to set
	 */
	public synchronized void deleteBackedupFile(String filename) {
		backedupFiles.remove(filename);
	}

	/**
	 * @return the chunksBeingSaved
	 */
	public synchronized ArrayList<Chunk> getChunksBeingSaved() {
		return chunksBeingSaved;
	}

	/**
	 * @param chunksBeingSaved the chunksBeingSaved to set
	 */
	public synchronized void setChunksBeingSaved(ArrayList<Chunk> chunksBeingSaved) {
		this.chunksBeingSaved = chunksBeingSaved;
	}

	/**
	 * @param chunk Chunk
	 */
	public synchronized void addChunkBeingSaved(Chunk chunk) {
		chunksBeingSaved.add(chunk);
	}
	
	/**
	 * @param chunk Chunk
	 */
	public synchronized Chunk getChunkBeingSaved(String fileHash, int chunkID) {
		
		for(int i = 0; i < chunksBeingSaved.size(); i++) {
			if(chunksBeingSaved.get(i).getFileID().equals(fileHash) && chunksBeingSaved.get(i).getId() == chunkID)
				return chunksBeingSaved.get(i);
		}
		
		return null;
	}
	
	public synchronized String getLog_dir() {
		return log_dir;
	}

	public String getRestore_dir() {
		return restore_dir;
	}
	
	public synchronized void setLog_dir(String log_dir) {
		this.log_dir = log_dir;
	}

	public synchronized void setRestore_dir(String restore_dir) {
		this.restore_dir = restore_dir;
		
	}
	
	
}
