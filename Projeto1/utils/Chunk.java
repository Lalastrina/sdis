package utils;

import java.util.ArrayList;

public class Chunk {

	public static int MAX_SIZE = 64000;//500;
	private String idHex;
	private int id;
	private String fileID;
	private int replicationDegree;
	private int actualReplicationDegree;
	private byte[] data;
	private int length;
	private ArrayList<String> confirmedPeers;
	
	public Chunk(int id, String fileID, int repDeg, byte[] data) {
		
		this.id = id;
		this.setIdHex(Integer.toHexString(id));
		this.confirmedPeers = new ArrayList<String>();
		this.fileID = fileID;
		this.replicationDegree = repDeg;
		this.setActualReplicationDegree(0);
		this.data = data;	
		if(data != null) 
			this.length = data.length;
		else 
			this.length = 0;
	}
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the fileID
	 */
	public String getFileID() {
		return fileID;
	}

	/**
	 * @param fileID the fileID to set
	 */
	public void setFileID(String fileID) {
		this.fileID = fileID;
	}

	/**
	 * @return the replicationDegree
	 */
	public int getReplicationDegree() {
		return replicationDegree;
	}

	/**
	 * @param replicationDegree the replicationDegree to set
	 */
	public void setReplicationDegree(int replicationDegree) {
		this.replicationDegree = replicationDegree;
	}

	/**
	 * @return the dataB
	 */
	public byte[] getDataB() {
		return data;
	}

	/**
	 * @param dataB the dataB to set
	 */
	public void setDataB(byte[] dataB) {
		this.data = dataB;
	}

	/**
	 * @return the actualReplicationDegree
	 */
	public int getActualReplicationDegree() {
		return actualReplicationDegree;
	}

	/**
	 * @param actualReplicationDegree the actualReplicationDegree to set
	 */
	public void setActualReplicationDegree(int actualReplicationDegree) {
		this.actualReplicationDegree = actualReplicationDegree;
	}	
	
	/**
	 * Increases replication degree
	 */
	public void increaseActualReplicationDegree() {
		this.actualReplicationDegree++;
	}
	
	/**
	 * Dencreases replication degree
	 */
	public void decreaseActualReplicationDegree() {
		this.actualReplicationDegree--;
	}
	
    /**
	 * @return the length
	 */
	public int getLength() {
		return length;
	}

	/**
	 * @param length the length to set
	 */
	public void setLength(int length) {
		this.length = length;
	}

	/**
	 * @return the confirmedPeers
	 */
	public ArrayList<String> getConfirmedPeers() {
		return confirmedPeers;
	}

	/**
	 * @param confirmedPeers the confirmedPeers to set
	 */
	public void setConfirmedPeers(ArrayList<String> confirmedPeers) {
		this.confirmedPeers = confirmedPeers;
	}

	/**
	 * @param confirmedPeer the confirmedPeer to add
	 */
	public void addConfirmedPeer(String confirmedPeer) {
		confirmedPeers.add(confirmedPeer);
	}

	/**
	 * @return the idHex
	 */
	public String getIdHex() {
		return idHex;
	}

	/**
	 * @param idHex the idHex to set
	 */
	public void setIdHex(String idHex) {
		this.idHex = idHex;
	}

	@Override
    public boolean equals(Object object)
    {
        boolean sameSame = false;

        if (object != null && object instanceof Chunk)
        {
            sameSame = (this.fileID.equals(((Chunk) object).getFileID()) && this.id == ((Chunk) object).getId());//this.id.equals(((Chunk) object).getId()));
        }

        return sameSame;
    }
	
}
