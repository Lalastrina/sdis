package protocols;

import java.io.IOException;
import java.net.DatagramPacket;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Random;

import main.InitiatorPeer;
import utils.Chunk;
import utils.FileUtil;
import utils.PeerMessage;

public class BackupChannel extends Channel {
	
	private static final int MAX_TRIES = 5;
	
	private Chunk chunk; //current chunk being sent
	private ArrayList<PeerMessage> confirmMessages; //STORED messages received to analyze
	private ArrayList<PeerMessage> confirmPUTCHUNKMessages; //PUTCHUNK messages received to analyze (used for space reclaim)
	private boolean isWaitingForRemoved;
	
	public BackupChannel(int port, String address) {
		
		super(port, address);
		confirmMessages = new ArrayList<PeerMessage>();
		setWaitingForRemoved(false);
		confirmPUTCHUNKMessages = new ArrayList<PeerMessage>();
	}
	
	/*
	 * Send message: PUTCHUNK <Version> <FileId> <ChunkNo> <ReplicationDeg> <CRLF><CRLF><Body>
	 * to the MDB channel.
	 * 
	 * @param chunk - chunk to send
	 * 
	 */
	@Override
	public void sendMessage() {

		System.out.println("Sending chunk: " + chunk.getId());
		String messageHeader = "PUTCHUNK " + Channel.VERSION + " " + chunk.getFileID() + " " 
							+ chunk.getId() + " " + chunk.getReplicationDegree() + " " 
							+ Channel.CRLF + Channel.CRLF;

		byte[] messageHeaderB = messageHeader.getBytes(StandardCharsets.US_ASCII);
		byte[] messageBody = chunk.getDataB();
		byte[] message = new byte[messageHeaderB.length + messageBody.length];		
		//merge body and header
		System.arraycopy(messageHeaderB, 0, message, 0, messageHeaderB.length);
		System.arraycopy(messageBody, 0, message, messageHeaderB.length, messageBody.length);
		
		DatagramPacket dataPacket = new DatagramPacket(message, message.length, address, port);
		
		int attempt = 0;
		int confirmReceived = chunk.getActualReplicationDegree();
		int sleepTime = 500; //half a second
		ArrayList<String> confirmedPeers = new ArrayList<String>();
		setConfirmMessages(new ArrayList<PeerMessage>());
		
		while(attempt < MAX_TRIES) {

			try {
				socket.send(dataPacket);
			} catch (IOException e) {
				System.out.println("Error occured while trying to send chunk number " + chunk.getId() + ". Size: " + message.length);
				e.printStackTrace();
			}

			try {
			    Thread.sleep(sleepTime);
			} catch(InterruptedException ex) {
				System.out.println("BackupChannel: sendMessage.");
			    Thread.currentThread().interrupt();
			}
			
			//check STORED messages received
			for(int i = 0; i < getConfirmMessages().size(); i++) {
				
				PeerMessage pmessage = getConfirmMessages().get(i);
				
				if(pmessage.getMessage()[2].equals(chunk.getFileID())) {
					if(Integer.parseInt(pmessage.getMessage()[3]) == chunk.getId()) {
						
						boolean found = false;
						int i1;
						//check if this peer confirmed already
						for(i1 = 0; i1 < confirmedPeers.size(); i1++) {
							if(confirmedPeers.get(i1).equals(pmessage.getOrigin())) {
								found = true;
								break;
							}
						}
						
						//peer didn't confirm, so let's accept it
						if(!found) {
							confirmReceived++;
							confirmedPeers.add(pmessage.getOrigin());
						}
					}
				}
			}
			
			deleteConfirmMessages();
			
			System.out.println("Confirmed: " + confirmReceived);
			
			if(confirmReceived >= chunk.getReplicationDegree()) {
				break;
			}
			else {
				sleepTime *= 2;
				attempt++;
			}			
		}
		
		if(attempt == MAX_TRIES) {
			System.out.println("Backup service exceed maximum attempts (5) for chunk: " + chunk.getId() + " with replication degree " + confirmReceived);
		}
		else {
			System.out.println("Sent chunk " + chunk.getId() + " with replication degree " + confirmReceived);
		}
	}

	/**
	 * @return the chunk
	 */
	public Chunk getChunk() {
		return chunk;
	}

	/**
	 * @param chunk the chunk to set
	 */
	public void setChunk(Chunk chunk) {
		this.chunk = chunk;
	}

	@Override
	public boolean listen() {
		
		//PUTCHUNK <Version> <FileId> <ChunkNo> <ReplicationDeg> <CRLF><CRLF><Body>
		byte[] buf = new byte[Channel.MAX_DATAGRAM];
		DatagramPacket serviceInfo = new DatagramPacket(buf, buf.length);
		
		try {
			socket.receive(serviceInfo);
		} catch (IOException e) {
			System.out.println("MDB channel appears to be closed.");
			this.setOpen(false);
		} finally {
			
			if(!this.isOpen()) {
				return false;
			}
		}	
		
		if(isWaitingForRemoved())
			interpretMessage2(serviceInfo);
		else
			interpretMessage(serviceInfo);
		
		return true;

	}

	public void interpretMessage2(DatagramPacket serviceInfo) {
		
		String packetOrigin = serviceInfo.getAddress().toString();
		
		if(!packetOrigin.equals(myAddress)) {
			String msg = new String(serviceInfo.getData(), 0, serviceInfo.getLength(), StandardCharsets.US_ASCII);
			String[] message = msg.split("[\\r\\n]+");	
			int headerSize = message[0].getBytes(StandardCharsets.US_ASCII).length;
			String[] header = message[0].trim().split("\\s+");

			System.out.println("RECEIVE BACKUP\t " + message[0]);
			
			if(header.length > 4 && header[0].equals("PUTCHUNK") && header[1].equals(Channel.VERSION)) {
				
				byte[] data = new byte[serviceInfo.getLength() - headerSize - 4];
				System.arraycopy(serviceInfo.getData(), headerSize + 4, data, 0, data.length);
				PeerMessage confirm = new PeerMessage(header, packetOrigin);
				addConfirmPUTCHUNKMessage(confirm);				
			}
			else {
					System.out.println("BACKUP: Message received but couldn't understand it: " + message[0]);
			}

		}

	}
	
	public void interpretMessage(DatagramPacket serviceInfo) {
		
		if(!serviceInfo.getAddress().toString().equals(myAddress)) {
			String msg = new String(serviceInfo.getData(), 0, serviceInfo.getLength(), StandardCharsets.US_ASCII);
			String[] message = msg.split("[\\r\\n]+");	
			int headerSize = message[0].getBytes(StandardCharsets.US_ASCII).length;
			String[] header = message[0].trim().split("\\s+");
			
			if(header.length > 4 && header[0].equals("PUTCHUNK") && header[1].equals(Channel.VERSION)) {
				
				byte[] data = new byte[serviceInfo.getLength() - headerSize - 4];
				System.arraycopy(serviceInfo.getData(), headerSize + 4, data, 0, data.length);

				Chunk chunk = new Chunk(Integer.parseInt(header[3]), header[2], Integer.parseInt(header[4]), data);
				
				if(chunk.getDataB().length + InitiatorPeer.getUsedSpace() > InitiatorPeer.getMaxSpace()) {
					System.out.println("No space to save this chunk");
				}
				else if(InitiatorPeer.logFile.hasFileBackedByFileHash(chunk.getFileID())) {
					System.out.println("Someone is initiating the backup subprotocol for one of my files.");
				}
				else {
					
					if(!InitiatorPeer.logFile.getChunksBeingSaved().contains(chunk)) {
						chunk.increaseActualReplicationDegree();
						InitiatorPeer.logFile.addChunkBeingSaved(chunk);	
						FileUtil.saveChunk(chunk, InitiatorPeer.getChunkDir());
						InitiatorPeer.increaseSpaceBeingUsed(chunk.getLength());
						InitiatorPeer.logFile.saveChunksBeingBackedUp();
					}
							
					String response = "STORED " + Channel.VERSION + " " + chunk.getFileID() + " " + chunk.getId() + Channel.CRLF + Channel.CRLF;

					Random random = new Random();
			        int timeToSleep = random.nextInt(401);//0 and 400
			        
			        try {
						Thread.sleep(timeToSleep);
					} catch (InterruptedException e) {
					    Thread.currentThread().interrupt();
					}
			        
			        InitiatorPeer.commandChannel.setMessage(response);
			        InitiatorPeer.commandChannel.sendMessage();
				}
				
			}
			else {
					System.out.println("BACKUP: Message received but couldn't understand it: " + message[0]);
			}

		}
	}
	
	/**
	 * @return the confirmMessages
	 */
	public synchronized ArrayList<PeerMessage> getConfirmMessages() {
		return confirmMessages;
	}

	/**
	 * @param confirmMessages the confirmMessages to set
	 */
	public void setConfirmMessages(ArrayList<PeerMessage> confirmMessages) {
		this.confirmMessages = confirmMessages;
	}
	
	/**
	 * @param confirmMessage
	 */
	public synchronized void addConfirmMessage(PeerMessage confirmMessage) {
		this.confirmMessages.add(confirmMessage);
	}
	
	/**
	 * @return the confirmPUTCHUNKMessages
	 */
	public synchronized ArrayList<PeerMessage> getConfirmPUTCHUNKMessages() {
		return confirmPUTCHUNKMessages;
	}

	/**
	 * @param confirmPUTCHUNKMessages the confirmPUTCHUNKMessages to set
	 */
	public synchronized void setConfirmPUTCHUNKMessages(ArrayList<PeerMessage> confirmPUTCHUNKMessages) {
		this.confirmPUTCHUNKMessages = confirmPUTCHUNKMessages;
	}
	
	/**
	 * @param confirmPUTCHUNKMessages the confirmPUTCHUNKMessages to set
	 */
	public synchronized void addConfirmPUTCHUNKMessage(PeerMessage confirmPUTCHUNKMessage) {
		this.confirmPUTCHUNKMessages.add(confirmPUTCHUNKMessage);
	}

	/**
	 * @param i index
	 * @return confirmMessage
	 */
	public synchronized PeerMessage getConfirmMessage(int i) {
		return this.confirmMessages.get(i);
	}
	
	/**
	 * 
	 */
	public synchronized void deleteConfirmMessages() {
		if(!confirmMessages.isEmpty())
			confirmMessages.clear();
	}

	/**
	 * @return the isWaitingForRemoved
	 */
	public synchronized boolean isWaitingForRemoved() {
		return isWaitingForRemoved;
	}

	/**
	 * @param isWaitingForRemoved the isWaitingForRemoved to set
	 */
	public synchronized void setWaitingForRemoved(boolean isWaitingForRemoved) {
		this.isWaitingForRemoved = isWaitingForRemoved;
	}
	
}
