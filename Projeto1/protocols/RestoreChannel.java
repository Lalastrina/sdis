package protocols;

import java.io.IOException;
import java.net.DatagramPacket;
import java.nio.charset.StandardCharsets;

import main.InitiatorPeer;
import utils.Chunk;
import utils.PeerMessage;

public class RestoreChannel extends Channel {

	private byte[] message;
	
	public RestoreChannel(int port, String address) {
		super(port, address);
	}

	@Override
	public void sendMessage() {
		
		System.out.println("SEND RESTORE\t" + message);
	
		DatagramPacket dataPacket = new DatagramPacket(message, message.length, address, port);
		
		try {
			socket.send(dataPacket);
		} catch (IOException e) {
			System.out.println("Error occured while trying to send message " + message);
			this.message = null;
			e.printStackTrace();
		}
		
		this.message = null;	
		
	}

	@Override
	public boolean listen() {
		//PUTCHUNK <Version> <FileId> <ChunkNo> <ReplicationDeg> <CRLF><CRLF><Body>
		byte[] buf = new byte[Channel.MAX_DATAGRAM];
		DatagramPacket serviceInfo = new DatagramPacket(buf, buf.length);
		
		try {
			socket.receive(serviceInfo);
		} catch (IOException e) {
			System.out.println("MDB channel appears to be closed.");
			this.setOpen(false);
		} finally {
			
			if(!this.isOpen()) {
				return false;
			}
		}	
		
		interpretMessage(serviceInfo);
		
		return true;
	}

	public void interpretMessage(DatagramPacket serviceInfo) {
		
		String packetOrigin = serviceInfo.getAddress().toString();
		
		if(!packetOrigin.equals(myAddress)) {
			String msg = new String(serviceInfo.getData(), 0, serviceInfo.getLength(), StandardCharsets.US_ASCII);
			String[] message = msg.split("[\\r\\n]+");	
			int headerSize = message[0].getBytes(StandardCharsets.US_ASCII).length;
			String[] header = message[0].trim().split("\\s+");

			System.out.println("RECEIVE RESTORE\t " + message[0]);
			
			if(header.length > 3 && header[0].equals("CHUNK") && header[1].equals(Channel.VERSION)) {
						
				Chunk chunk = InitiatorPeer.commandChannel.getChunkBeingRestored();
				
				//we are wanting to restore this file, so keep it

				if(chunk != null && header[2].equals(chunk.getFileID()) && (Integer.parseInt(header[3]) == chunk.getId())) {
					
					byte[] data = new byte[serviceInfo.getLength() - headerSize - 4];
					System.arraycopy(serviceInfo.getData(), headerSize + 4, data, 0, data.length);
					
					//Chunk chunk1 = new Chunk(header[3], header[2], Integer.parseInt(header[4]), data);
					InitiatorPeer.commandChannel.getChunkBeingRestored().setDataB(data);//FileUtil.addChunkToFile(header[2], data);
					InitiatorPeer.setWaiting(false);
					
				}
				//we are just counting how many CHUNK we got
				else {
					InitiatorPeer.commandChannel.addConfirmChunkMessage(new PeerMessage(header, packetOrigin));
				}
				
			}
			else {
				System.out.println("RESTORE: Message received but couldn't understand it: " + message[0]);
			}

		}
	}

	/**
	 * @return the message
	 */
	public byte[] getMessage() {
		return message;
	}

	/**
	 * @param message the message to set
	 */
	public synchronized void setMessage(byte[] message) {
		this.message = message;
	}

}
