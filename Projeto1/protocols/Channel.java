package protocols;

import java.io.IOException;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.UnknownHostException;

public abstract class Channel {

	private static final int newttl = 1;
	public static final int MAX_DATAGRAM = 65536;//64KBytes
	public static String VERSION = "1.0";
	public static String CRLF = "\r\n";
	
	private boolean isOpen;
	private int ttl;
	protected InetAddress address;
	protected int port;
	protected MulticastSocket socket;
	protected String myAddress;
	protected String myHost;
	
	public Channel(int port, String address) {
		
		this.port = port;
		this.setOpen(true);
		try {
			this.myAddress = "/" + InetAddress.getLocalHost().getHostAddress();
			this.myHost = InetAddress.getLocalHost().getHostName();
		} catch (UnknownHostException e1) {
			e1.printStackTrace();
		}
		
		try {
			this.address = InetAddress.getByName(address);
			socket = new MulticastSocket(port);
			ttl = socket.getTimeToLive(); 
			socket.setTimeToLive(newttl);
			socket.joinGroup(this.address);
			
		} catch (IOException e) {
			System.out.println("Error opening multicast socket: " + port + " " + address);
			e.printStackTrace();
		}
	}
	
	public void closeCommunication() {
		
		try {
			socket.setTimeToLive(ttl);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		socket.close();
		this.setOpen(false);
	}
	
	public abstract void sendMessage();	
	public abstract boolean listen();

	/**
	 * @return the isOpen
	 */
	public boolean isOpen() {
		return isOpen;
	}

	/**
	 * @param isOpen the isOpen to set
	 */
	public void setOpen(boolean isOpen) {
		this.isOpen = isOpen;
	}
	
	
}
