package protocols;

import java.util.ArrayList;

import main.InitiatorPeer;
import utils.Chunk;

public class Helper extends Thread {

	private boolean running = true;
	private ArrayList<Chunk> tasks = new ArrayList<Chunk>();
	
	@Override
	public void run() {
		super.run();	
		
		while(running) {
			
			if(!tasks.isEmpty()) {
				treatTask(tasks.get(0));
				tasks.remove(0);
			}
			else {
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
				    Thread.currentThread().interrupt();
				}
			}
		}
	}
	
	public void treatTask(Chunk chunk) {
		
		InitiatorPeer.backupChannel.setChunk(chunk);
		InitiatorPeer.backupChannel.sendMessage();	   
		
	}
	
	public synchronized void addTask(Chunk peerMessage) {
		tasks.add(peerMessage);
	}

	/**
	 * @return the running
	 */
	public boolean isRunning() {
		return running;
	}

	/**
	 * @param running the running to set
	 */
	public synchronized void setRunning(boolean running) {
		this.running = running;
	}

}
