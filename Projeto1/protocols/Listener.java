package protocols;

public class Listener extends Thread {

	private Channel channel;
	volatile boolean running = false;
	
	public Listener(Channel channel) {
		
		this.channel = channel;
		
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Thread#run()
	 */
	@Override
	public void run() {
		
		super.run();
		this.running = true;
		
		while(running) {

			running = channel.listen();

			
		}
		
	}
	
    public void stopRunning() {
        running = false;
    }
	
}
