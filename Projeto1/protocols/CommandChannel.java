package protocols;

import java.io.IOException;
import java.net.DatagramPacket;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Random;

import main.InitiatorPeer;
import utils.Chunk;
import utils.FileUtil;
import utils.PeerMessage;

public class CommandChannel extends Channel {

	private String message;
	private ArrayList<PeerMessage> confirmChunkMessages; //CHUNK messages received to analyze
	private Chunk chunkBeingRestored;
	private Helper helper;
	
	public CommandChannel(int port, String address) {
		
		super(port, address);
		confirmChunkMessages = new ArrayList<PeerMessage>();
		setChunkBeingRestored(null);
		helper = new Helper();
		helper.start();
	}

	@Override
	public boolean listen() {
		
		byte[] buf = new byte[Channel.MAX_DATAGRAM];
		DatagramPacket serviceInfo = new DatagramPacket(buf, buf.length);
		
		try {
			socket.receive(serviceInfo);
		} catch (IOException e) {
			System.out.println("MC channel appears to be closed.");
			this.setOpen(false);
		} finally {
			
			if(!this.isOpen()) {
				return false;
			}
		}	
		
		interpretMessage(serviceInfo);
		
		return true;
	
	}

	@Override
	public void sendMessage() {
		
		System.out.println("SEND COMMAND\t" + message);
		byte[] messageB = message.getBytes(StandardCharsets.US_ASCII);
		DatagramPacket dataPacket = new DatagramPacket(messageB, messageB.length, address, port);
		
		try {
			socket.send(dataPacket);
		} catch (IOException e) {
			System.out.println("Error occured while trying to send message " + message);
			this.message = null;
			e.printStackTrace();
		}
		
		this.message = null;		
	}

	public void interpretMessage(DatagramPacket serviceInfo) {
		
		String packetOrigin = serviceInfo.getAddress().toString();
		
		if(!packetOrigin.equals(myAddress)) {
			String msg = new String(serviceInfo.getData(), 0, serviceInfo.getLength(), StandardCharsets.US_ASCII);

			String[] info = msg.trim().split("\\s+");
			System.out.println("RECEIVE COMMAND\t " + msg);
			
			if(info[1].equals(Channel.VERSION)) {
	
				if(info.length > 3 && info[0].equals("STORED")) {
					//STORED - STORED <Version> <FileId> <ChunkNo> <CRLF><CRLF>
					treatStored(info, packetOrigin);
				}
				else if(info.length > 3 && info[0].equals("GETCHUNK")) {
					//GETCHUNK - GETCHUNK <Version> <FileId> <ChunkNo> <CRLF><CRLF>
					treatGetChunk(info);
				}
				else if(info.length > 2 && info[0].equals("DELETE")) {
					treatDelete(info);
				}
				else if(info.length > 2 && info[0].equals("REMOVED")) {
					treatRemoved(info, packetOrigin);
				}
				else {
					System.out.println("COMMAND: Message received but couldn't understand it: " + msg);
				}
				
			}
		}
		
	}
	
	public void treatStored(String[] info, String packetOrigin) {
		//if we are sending this chunk, confirm that a peer has stored it
		InitiatorPeer.backupChannel.addConfirmMessage(new PeerMessage(info, packetOrigin));
		
		//if we are storing this chunk, increase the replication degree
		ArrayList<Chunk> chunksBeingSaved = InitiatorPeer.logFile.getChunksBeingSaved();
		
		for(int i = 0; i < chunksBeingSaved.size(); i++) {
			if(chunksBeingSaved.get(i).getFileID().equals(info[2]) && String.valueOf(chunksBeingSaved.get(i).getId()).equals(info[3])) {
				//we check to see if this peer confirmed for this chunk already, so that we don't increase the replication degree too much
				//TODO  is needed? no? backupChannel > interpretMessage(DatagramPacket serviceInfo)
				if(!chunksBeingSaved.get(i).getConfirmedPeers().contains(packetOrigin)) {
					chunksBeingSaved.get(i).increaseActualReplicationDegree();
					chunksBeingSaved.get(i).addConfirmedPeer(packetOrigin);
					InitiatorPeer.logFile.saveChunksBeingBackedUp();
				}
			}
		}
		
	}
	
	public void treatGetChunk(String[] info) {
		
		ArrayList<Chunk> chunksBeingSaved = InitiatorPeer.logFile.getChunksBeingSaved();

		//check to see if we have this chunk
		for(int i = 0; i < chunksBeingSaved.size(); i++) {
			
			String fileHash = chunksBeingSaved.get(i).getFileID();
			String id = String.valueOf(chunksBeingSaved.get(i).getId());
			
			if(fileHash.equals(info[2]) && id.equals(info[3])) {
				
				//we have this chunk, let's sleep
				Random random = new Random();
		        int timeToSleep = random.nextInt(401);//0 and 400
		        
				try {
				    Thread.sleep(timeToSleep);
				} catch(InterruptedException ex) {
				    Thread.currentThread().interrupt();
				}
				
				//check if other peers sent the chunk
				boolean found = false;
				
				for(PeerMessage pmessage : getConfirmChunkMessages()) {
					
					if(pmessage.getMessage()[2].equals(fileHash)) {
						if(pmessage.getMessage()[3].equals(id)) {
							found = true;
							break;
						}
					}
				}
				
				deleteConfirmChunkMessages();
				
				//no other peers sent the chunk
				if(!found) {
					
					byte[] data = FileUtil.getChunkData(info[2], info[3], InitiatorPeer.getChunkDir());
					String message = "CHUNK " + Channel.VERSION + " " + info[2] + " " + info[3] + " " + Channel.CRLF + Channel.CRLF;
					byte[] messageB = message.getBytes(StandardCharsets.US_ASCII);
					byte[] messageB1 = new byte[data.length + messageB.length];

					System.arraycopy(messageB, 0, messageB1, 0, messageB.length);
					System.arraycopy(data, 0, messageB1, messageB.length, data.length);
				
					InitiatorPeer.restoreChannel.setMessage(messageB1);
					InitiatorPeer.restoreChannel.sendMessage();		
					//TODO chunksBeingSaved.get(i).SENT
				}
				
				break;
			}
		}
	}
	
	public void treatDelete(String[] info) {
	
		String fileHash = info[2];
		
		ArrayList<Chunk> chunks = new ArrayList<Chunk>();
		System.out.println("Trying to delete chunks belonging to a file.");
		
		for(Chunk chunk : InitiatorPeer.logFile.getChunksBeingSaved()) {

		    if(chunk.getFileID().equals(fileHash)) { 
				if(FileUtil.deleteChunk(chunk.getFileID()+"."+chunk.getId(),InitiatorPeer.getChunkDir())) {
					InitiatorPeer.decreaseSpaceBeingUsed(chunk.getLength());
					System.out.println("Deleted a chunk belonging to this file.");
				}
				else
					System.out.println("Couldn't delete a chunk belonging to this file.");
			}
		    else
		    	chunks.add(chunk);
		    
		}
		
		InitiatorPeer.logFile.setChunksBeingSaved(chunks);
		InitiatorPeer.logFile.saveChunksBeingBackedUp();

	}
	
	public void treatRemoved(String[] info, String packetOrigin) {
		
		Chunk chunk = InitiatorPeer.logFile.getChunkBeingSaved(info[2], Integer.parseInt(info[3]));
		
		System.out.println("A chunk was removed from the system, checking...");
		
		//if we have this chunk
		if(chunk != null) {
			
			//let's decrease its rep degree since it was removed and all that jazz.
	    	chunk.decreaseActualReplicationDegree();
	    	if(chunk.getConfirmedPeers().contains(packetOrigin))
	    		chunk.getConfirmedPeers().remove(packetOrigin);
	    	
	    	InitiatorPeer.logFile.saveChunksBeingBackedUp();
	    	
	    	if(chunk.getActualReplicationDegree() < chunk.getReplicationDegree()) {
	    		//let's sleep and wait to see if someone already took care of this
				InitiatorPeer.backupChannel.setWaitingForRemoved(true);
				Random random = new Random();
		        int timeToSleep = random.nextInt(401);//0 and 400
		        
				try {
				    Thread.sleep(timeToSleep);
				} catch(InterruptedException ex) {
				    Thread.currentThread().interrupt();
				}
				
				InitiatorPeer.backupChannel.setWaitingForRemoved(false);
				//if no one else sent PUTCHUNK, we will try
				if(InitiatorPeer.backupChannel.getConfirmPUTCHUNKMessages().isEmpty()) {		    		
			    		InitiatorPeer.backupChannel.setConfirmPUTCHUNKMessages(new ArrayList<PeerMessage>());
			    		chunk.setDataB(FileUtil.getChunkData(chunk.getFileID(), chunk.getId(), InitiatorPeer.getChunkDir()));		    		
			    		helper.addTask(chunk);
				}
				
	    	}
	    	else
	    		System.out.println("All is in order.");
			
		}
		else
			System.out.println("We don't have this chunk.");
		
	}
	
	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message the message to set
	 */
	public synchronized void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @return the confirmChunkMessages
	 */
	public synchronized ArrayList<PeerMessage> getConfirmChunkMessages() {
		return confirmChunkMessages;
	}

	/**
	 * @return the confirmChunkMessages
	 */
	public synchronized void deleteConfirmChunkMessages() {
		if(!confirmChunkMessages.isEmpty())
			confirmChunkMessages.clear();
	}

	
	/**
	 * @param confirmChunkMessages the confirmChunkMessages to set
	 */
	public synchronized void setConfirmChunkMessages(ArrayList<PeerMessage> confirmChunkMessages) {
		this.confirmChunkMessages = confirmChunkMessages;
	}

	/**
	 * @param confirmChunkMessage the confirmChunkMessage to add
	 */
	public synchronized void addConfirmChunkMessage(PeerMessage confirmChunkMessage) {
		this.confirmChunkMessages.add(confirmChunkMessage);
	}

	/**
	 * @return the chunkBeingRestored
	 */
	public synchronized Chunk getChunkBeingRestored() {
		return chunkBeingRestored;
	}

	/**
	 * @param chunkBeingRestored the chunkBeingRestored to set
	 */
	public synchronized void setChunkBeingRestored(Chunk chunkBeingRestored) {
		this.chunkBeingRestored = chunkBeingRestored;
	}

	/**
	 * @return the helper
	 */
	public Helper getHelper() {
		return helper;
	}
	
}
