package main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.UnknownHostException;

public class Main {
	
	public static void main(String[] args) {
		
		if(args.length != 6) {
			System.out.println("Usage: java Main mcIP mcPort mdbIP mdbPort mdrIP mdrPort");
			System.exit(0);
		}
		
		try {
			System.out.println("This address: " + InetAddress.getLocalHost().getHostAddress() + "\n" 
								+ "Command channel: " + args[0] + " " + args[1] + "\n"
								+ "MDB channel: " + args[2] + " " + args[3] + "\n"
								+ "MDR channel: " + args[4] + " " + args[5] + "\n");
		} catch (UnknownHostException e1) {
			e1.printStackTrace();
		}
		
		boolean run = true;
		BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
		String command = null;
		String[] commands;
		InitiatorPeer initiator = new InitiatorPeer(args);
		
		showHelpMenu();
		
		while(run) {
			
			System.out.print("Input your command: ");
			command = null;
			
			try {
				command = input.readLine();
			} catch (IOException e) {
				System.out.println("Error reading input.");
				e.printStackTrace();
			}
			
			commands = command.split(" ");
			
			if(commands[0].toLowerCase().equals("h"))
				showHelpMenu();
			else if(commands[0].toLowerCase().equals("q")){
				run = false;
			}
			else if(commands[0].toLowerCase().equals("initiate")) {
				
				if(commands.length < 2 || commands.length > 4 || (commands[1].equals("-backup") && commands.length != 4) || (commands[1].equals("-reclaim") && commands.length != 2)) {
					System.out.println("Invalid number of arguments. Use h for command help.");
				}
				else {
					if(commands[1].toLowerCase().equals("-backup")) {
						
						String filename = commands[2];
						int replicationDegree = Integer.parseInt(commands[3]);
						
						if(replicationDegree < 1 || replicationDegree > 9999) {
							System.out.println("Invalid replication degree. Must be between 1 and 9999.");
						}
						else {
							System.out.println("********************");
							initiator.setCurrentCommand(InitiatorPeer.CommandType.BACKUP);
							if(initiator.backupFile(filename, replicationDegree))
								System.out.println("Finished backing up file: " + filename);
							System.out.println("********************");
						}												
					}
					else if(commands[1].toLowerCase().equals("-restore")) {

						String filename = commands[2];

						System.out.println("********************");
						initiator.setCurrentCommand(InitiatorPeer.CommandType.RESTORE);
						if(initiator.restoreFile(filename))
							System.out.println("Finished restoring file: " + filename + ". If everything went fine, it's saved in the restore folder.");
						System.out.println("********************");
					
					}
					else if(commands[1].toLowerCase().equals("-reclaim")) {

						System.out.println("********************");
						initiator.setCurrentCommand(InitiatorPeer.CommandType.RECLAIM);
						if(initiator.spaceReclaim()) {
							System.out.println("Finished reclaiming space:");
							initiator.showInfo(false);
						}
						else {
							System.out.println("There isn't anything to delete right now.");
						}
						System.out.println("********************");
												
					}
					else if(commands[1].toLowerCase().equals("-delete")) {

						String filename = commands[2];
						System.out.println("********************");
						initiator.setCurrentCommand(InitiatorPeer.CommandType.DELETE);
						if(initiator.deleteFile(filename))
							System.out.println("Finished deleting file: " + filename + ".");
						System.out.println("********************");
												
					}
					else {
						System.out.println("Invalid command. Use h for help.");
					}
				}				
			}
			else if(commands[0].toLowerCase().equals("files")) {
				System.out.println("These files have been backed up (sent chunks to peers):");				
				initiator.printFiles();
			}
			else if(commands[0].toLowerCase().equals("chunkdir")) {
				if(commands.length != 2)
					System.out.println("Invalid number of arguments.");	
				else
					InitiatorPeer.setChunkDir(commands[1]);
			}
			else if(commands[0].toLowerCase().equals("logdir")) {
				if(commands.length != 2)
					System.out.println("Invalid number of arguments.");	
				else
					InitiatorPeer.logFile.setLog_dir(commands[1]);
			}
			else if(commands[0].toLowerCase().equals("restoredir")) {
				if(commands.length != 2)
					System.out.println("Invalid number of arguments.");	
				else
					InitiatorPeer.logFile.setRestore_dir(commands[1]);
			}
			else if(commands[0].toLowerCase().equals("maxspace")) {
				if(commands.length != 2)
					System.out.println("Invalid number of arguments.");	
				else
					InitiatorPeer.setMaxSpace(Integer.parseInt(commands[1]), true);
			}
			else if(commands[0].toLowerCase().equals("info")) {
					initiator.showInfo(true);				
			}
			else if(commands[0].toLowerCase().equals("check")) {
				if(commands.length != 2)
					System.out.println("Invalid number of arguments.");	
				else if(commands[1].toLowerCase().equals("deleted")){
					if(initiator.checkForDeletedFiles())
						System.out.println("Checked for deleted files successful. Some chunks were removed.");
					else
						System.out.println("No files needed to be deleted.");
				}
			}
			else {
				System.out.println("Invalid command.");
			}
		}
		
		System.out.println("Closing...");
		initiator.exit();
		System.exit(0);

	}
	
	public static void showHelpMenu() {
		
		System.out.println("Hello. Here are some useful commands: ");
		System.out.println("Command \t\t\t\t Effect\n");
		System.out.println("q \t\t\t\t\t Quit.\n" +
				"h \t\t\t\t\t Shows this pretty help menu.\n" +
				"files \t\t\t\t\t Shows files that have been backed up.\n\n" +
				"chunkdir dir \t\t\t\t Change directory where chunks are stored\n" +
				"logdir dir \t\t\t\t Change directory where logs are stored\n" +
				"restore dir \t\t\t\t Change directory where restored files are stored\n" +
				"maxspace space \t\t\t\t Change max space to 'space'\n\n" +
				"info \t\t\t\t\t Shows information\n" +
				"initiate -backup filename repDegree \t Initiates backup of filename with replicationDegree\n" +
				"initiate -restore filename \t\t Initiates restore of filename\n" +
				"initiate -delete filename \t\t Initiates deletion of filename\n" +
				"initiate -reclaim \t\t\t Starts reclaiming space occupied\n" + 
				"check deleted\n");
		
	}

}