package main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import protocols.Channel;
import protocols.Listener;
import protocols.BackupChannel;
import protocols.CommandChannel;
import protocols.RestoreChannel;
import utils.Chunk;
import utils.FileUtil;
import utils.Log;

public class InitiatorPeer {

	public enum CommandType {BACKUP, DELETE, RESTORE, RECLAIM};
	
	public static BackupChannel backupChannel;
	public static CommandChannel commandChannel;
	public static RestoreChannel restoreChannel;
	public static Log logFile;
	public static ArrayList<Chunk> chunksToBeRestored;
	private static boolean waiting;
	private static String chunkDir;
	private static int maxSpace = -1;
	private static int usedSpace = -1;
	private Listener l1;
	private Listener l2;
	private Listener l3;	
	private CommandType currentCommand;
	
	public InitiatorPeer(String[] info) {

		chunkDir = "chunks";
		logFile = new Log();
		backupChannel = new BackupChannel(Integer.parseInt(info[3]), info[2]);
		commandChannel = new CommandChannel(Integer.parseInt(info[1]), info[0]);
		restoreChannel = new RestoreChannel(Integer.parseInt(info[5]), info[4]);
		chunksToBeRestored = new ArrayList<Chunk>();
		if(maxSpace == -1) maxSpace = 6400000;
		if(usedSpace == -1) usedSpace = 0;
		
		l1 = new Listener(backupChannel);
		l2 = new Listener(commandChannel);
		l3 = new Listener(restoreChannel);
		l1.start();
		l2.start();
		l3.start();
		
	}
	
	public InitiatorPeer(String[] info, String chunkDir1) {

		chunkDir = chunkDir1;
		logFile = new Log();
		backupChannel = new BackupChannel(Integer.parseInt(info[3]), info[2]);
		commandChannel = new CommandChannel(Integer.parseInt(info[1]), info[0]);
		restoreChannel = new RestoreChannel(Integer.parseInt(info[5]), info[4]);
		
		l1 = new Listener(backupChannel);
		l2 = new Listener(commandChannel);
		l3 = new Listener(restoreChannel);
		l1.start();
		l2.start();
		l3.start();
		
	}

	/**
	 * Checks if the file exists, creates its file id and
	 * divides it into chunks to send over the MDB channel.
	 * @param filename
	 */
	public boolean backupFile(String filename, int replicationDegree) {
		
		if(logFile.hasFileBacked(filename)) {
			
			boolean valid = false;
			String command = null;
			BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
			
			while(!valid) {
				
				System.out.println("This file has been backed up already. Do you wish to proceed? Y/N");
				
				try {
					command = input.readLine();
				} catch (IOException e) {
					System.out.println("Error reading input.");
					return false;
				}
				
				if(command.toUpperCase().equals("N")) {
					System.out.println("This file won't be backed up. Returning to main menu.");
					return false;
				}
				else if(command.toUpperCase().equals("Y")) {
					System.out.println("This file will be backed up...");
					break;
				}				
				
			}

		}
			
		FileUtil fileManager = new FileUtil(filename, replicationDegree);

		if(fileManager.treatFile()) {
			fileManager.generateEncryptedFileID();
			Chunk[] chunks = fileManager.getChunks();
			
			if(chunks.length > 0) {
				System.out.println("Backing up file " + filename + ". It has " + chunks.length + " chunks with replication degree of " + replicationDegree + " for each chunk.\n");
				
				for(Chunk chunk : chunks) {
					backupChannel.setChunk(chunk);
					backupChannel.sendMessage();
				}
				
				logFile.addFileToBacked(filename, fileManager.getFileID() + " " + chunks.length);
				logFile.saveBackedUp();
					
			}
			else {
				System.out.println("No chunks to backup for this file.");
				return false;
			}
		}
		else {
			System.out.println("File doesn't exist. Can't backup what doesn't exist.");
			return false;
		}
		
		return true;

	}
	
	/**
	 * Tries to retrieve all chunks belonging to a file
	 * @param filename
	 */
	public boolean restoreFile(String filename) {
		
		String value = logFile.getFileHash(filename);
		
		if(value != null) {
			System.out.println("Will try to restore this file: " + filename + "\n");
			
			String[] parts = value.split(" ");
			String fileHash = parts[0];
			int numberChunks = Integer.valueOf(parts[1]);
							
			for(int i = 0; i < numberChunks; i++) {
				setWaiting(true);
				int attempts = 0;
				String restore = "GETCHUNK " + Channel.VERSION + " " + fileHash + " " + i + " " + Channel.CRLF + Channel.CRLF;
				Chunk chunkRestore = null;
				
				while(isWaiting()) {
					
					commandChannel.setMessage(restore);
					chunkRestore = new Chunk(i, fileHash, -1, null);
					commandChannel.setChunkBeingRestored(chunkRestore);
					commandChannel.sendMessage();
					
					try {
						Thread.sleep(500);
					} catch (InterruptedException e) {
					    Thread.currentThread().interrupt();
					}
					
					if(attempts > 10) {
						System.out.println("Some error must have occured while trying to fetch this file. Perhaps no peer with this file is connected.");
						return false;
					}
					attempts++;
				}
				FileUtil.addChunkToFile(filename, chunkRestore.getDataB());
			}
			commandChannel.setChunkBeingRestored(null);
			return true;
		}
		else {
			System.out.println("No such file has been backed up recently. Here are the files that you can restore: \n");
			this.printFiles();
			System.out.println("Returning to main menu.");
			return false;
		}

	}
	
	/**
	 * Tries to delete all chunks belonging to a file
	 * @param filename
	 */
	public boolean deleteFile(String filename) {
		
		String value = logFile.getFileHash(filename);
		
		if(value != null) {
			System.out.println("Will try to delete this file: " + filename + "\n");
			
			String[] parts = value.split(" ");
			String fileHash = parts[0];
			int attempts = 3;
			int currentAttempt = 0;

			String delete = "DELETE " + Channel.VERSION + " " + fileHash + " " + Channel.CRLF + Channel.CRLF;
			
			while(currentAttempt < attempts) {
				
				commandChannel.setMessage(delete);
				commandChannel.sendMessage();
				currentAttempt++;
				
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
				    Thread.currentThread().interrupt();
				}	
			}
			
			logFile.deleteBackedupFile(filename);
			logFile.saveBackedUp();
			return true;
		}
		else {
			System.out.println("No such file has been backed up recently. Here are the files that you can delete: \n");
			this.printFiles();
			System.out.println("Returning to main menu.");
			return false;
		}
		
	}
	
	public boolean spaceReclaim() {
		
		System.out.println("This is the current used space and the max space allowed for this application:");
		this.showInfo(false);
		System.out.println("If you wish to change the current max space, you can do so in the main menu.\nChecking to see if we can delete anything...");
		
		ArrayList<Chunk> chunks = new ArrayList<Chunk>();
		boolean changedAnything = false;
		
		for(Chunk chunk : logFile.getChunksBeingSaved()) {
			
			if(chunk.getActualReplicationDegree() > chunk.getReplicationDegree()) {
				
				if(FileUtil.deleteChunk(chunk.getFileID()+"."+chunk.getId(), getChunkDir())) {
					changedAnything = true;
					decreaseSpaceBeingUsed(chunk.getLength());
					String removed = "REMOVED " + Channel.VERSION + " " + chunk.getFileID() + " " + chunk.getId() + Channel.CRLF + Channel.CRLF;
					commandChannel.setMessage(removed);
					commandChannel.sendMessage();
				}

			}
			else {
				chunks.add(chunk);
			}
		}

		if(changedAnything) {
			logFile.setChunksBeingSaved(chunks);
			logFile.saveChunksBeingBackedUp();
			System.out.println("CHANGED : " + logFile.getChunksBeingSaved().size());
			return true;
		}
		else {
			return false;
		}

	}
	
	public void showInfo(boolean showAll) {
		
		System.out.println("Current used space: " + InitiatorPeer.getUsedSpace());
		System.out.println("Max use space: " + InitiatorPeer.getMaxSpace());
		
		if(showAll) {
			System.out.println("Directories being used: \n" + 
					"\tChunks: " + InitiatorPeer.getChunkDir() + "\n" +
					"\tLogs: " + InitiatorPeer.logFile.getLog_dir());
			System.out.println("Files that were backed up from this peer: " + InitiatorPeer.logFile.getBackedupFiles().size());
			System.out.println("Chunks being saved in this peer: " + InitiatorPeer.logFile.getChunksBeingSaved().size());
		}

	}
	
	public void printFiles() {
		
		for (String key : logFile.getBackedupFiles().keySet()) {
		    System.out.println(key);
		}
	}
	
	/**
	 * @return the current command
	 */
	public CommandType getCurrentCommand() {
		return currentCommand;
	}

	/**
	 * @param currentCommand - a CommandType
	 */
	public void setCurrentCommand(CommandType currentCommand) {
		this.currentCommand = currentCommand;
	}
	
	/**
	 * @return the chunksToBeRestored
	 */
	public synchronized ArrayList<Chunk> getChunksBeingRestored() {
		return chunksToBeRestored;
	}

	/**
	 * @param chunk Chunk
	 */
	public synchronized void addChunkBeingRestored(Chunk chunk) {
		chunksToBeRestored.add(chunk);
	}
	
	/**
	 * @param chunk Chunk
	 */
	public synchronized void emptyChunksBeingRestored() {
		if(!chunksToBeRestored.isEmpty())
			chunksToBeRestored.clear();
	}
	
	public static String getChunkDir() {
		return chunkDir;
	}

	public static void setChunkDir(String chunkDir1) {
		chunkDir = chunkDir1;
	}

	public static boolean isWaiting() {
		return waiting;
	}

	public static void setWaiting(boolean waiting1) {
		waiting = waiting1;
	}

	/**
	 * @return the maxSpace
	 */
	public static int getMaxSpace() {
		return maxSpace;
	}

	/**
	 * @param maxSpace the maxSpace to set
	 */
	public static void setMaxSpace(int maxSpace, boolean save) {
		
		InitiatorPeer.maxSpace = maxSpace;
		
		if(save)
			logFile.saveLog();
			
	}

	/**
	 * @return the usedSpace
	 */
	public static int getUsedSpace() {
		return usedSpace;
	}

	/**
	 * @param usedSpace the usedSpace to set
	 */
	public static void setUsedSpace(int usedSpace) {
		InitiatorPeer.usedSpace = usedSpace;
	}

	public void exit() {
		backupChannel.closeCommunication();
		commandChannel.getHelper().setRunning(false);
		commandChannel.closeCommunication();
		restoreChannel.closeCommunication();
		try {
			commandChannel.getHelper().join();
			l1.join();
			l2.join();
			l3.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
	}

	public synchronized static void increaseSpaceBeingUsed(int length) {
		usedSpace += length;
		logFile.saveLog();
	}
	
	public synchronized static void decreaseSpaceBeingUsed(int length) {
		usedSpace -= length;
		logFile.saveLog();
	}
	
	public boolean checkForDeletedFiles() {
		
		System.out.println("Checking to see if files were deleted. This can take a bit...");
		
		if(logFile.getChunksBeingSaved().isEmpty()) 
			return false;
		
		int attempt = 0;
		int sleepTime;
		boolean found = false;
		ArrayList<String> filesChecked = new ArrayList<String>();//ja verifiquei e existe
		ArrayList<Chunk> newChunks = new ArrayList<Chunk>();
		boolean changedAnything = false;
		
		for(Chunk chunk : logFile.getChunksBeingSaved()) {
			
			if(!filesChecked.contains(chunk.getFileID())) {
				commandChannel.deleteConfirmChunkMessages();
				String restore = "GETCHUNK " + Channel.VERSION + " " + chunk.getFileID() + " " + chunk.getId() + " " + Channel.CRLF + Channel.CRLF;
				sleepTime = 500;
				
				while(attempt < 5) {
					
					try {
						commandChannel.setMessage(restore);
						commandChannel.sendMessage();
						Thread.sleep(sleepTime);
					} catch (InterruptedException e) {
					    Thread.currentThread().interrupt();
					}
					
					if(commandChannel.getConfirmChunkMessages().isEmpty()) {
						attempt++;
						sleepTime*=2;
					}
					else {
						found = true;
						break;
					}
				}
				
				//se receber chunk: adicionar a filesChecked e a newChunks	
				if(found) {//o chunk existe somewhere
					filesChecked.add(chunk.getFileID());
					newChunks.add(chunk);
				}							
			} 
			else {
				
				newChunks.add(chunk);
			}
			
		}
		
		for(Chunk chunk : logFile.getChunksBeingSaved()) {
			
			System.out.println("Chunk here: " + chunk.getFileID());
			if(!newChunks.contains(chunk)) {
				changedAnything = true;
				FileUtil.deleteChunk(chunk.getFileID()+"."+chunk.getId(), getChunkDir());
				decreaseSpaceBeingUsed(chunk.getLength());
			}
		}
		
		if(changedAnything) {
			logFile.setChunksBeingSaved(newChunks);
			logFile.saveChunksBeingBackedUp();
			return true;
		}
		else
			return false;
		
		
	}
}
