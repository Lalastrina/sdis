Sara Filipa Mendes da Silva - ei11096@fe.up.pt
Turma 2
Grupo 14


COMPILE
Pode ser compilado de maneira normal, sem configura��es adicionais.

RUN
Para correr o programa s�o necess�rios 6 argumentos iniciais: mcIP mcPort mdbIP mdbPort mdrIP mdrPort
A aplica��o inicia a partir do ficheiro Main.java na pasta main.

Ao iniciar o programa, pode correr os seguintes comandos:

	Ajuda

		h 	- Mostra o "help menu", uma list de comandos que podem ser usados.
		q 	- Sai do programa.
		files 	- Mostra ficheiros que foram "backed up" a partir deste peer.
		info 	- Mostra quantos ficheiros foram "backed up" a partir deste peer, quantos chunks tem guardados,
				espa�o ocupado, espa�o m�ximo para chunks e em que diretorios se encontram os logs e chunks.

	Mudar diretorios e espa�o disponivel

		chunkdir "dir" 		- Muda o diretorio onde os chunks s�o guardados para "dir" (n�o � uma mudan�a retroativa).
		logdir "dir" 		- Muda o diretorio onde os logs s�o guardados para "dir" (n�o � uma mudan�a retroativa).
		restoredir "dir" 	- Muda o diretorio onde os ficheiros restaurados s�o guardados (n�o � uma mudan�a retroativa).
		maxspace "space" 	- Muda o tamanho m�ximo de espa�o dispon�vel para chunks (n�o � uma mudan�a retroativa).

	Correr os protocolos
		
		initiate -backup "filename" "replicationDegree" - Inicia o backup de um ficheiro "filename" com "replicationDegree".
		initiate -restore "filename" 			- Inicia o restauro de um ficheiro "filename", se este foi backed up a partir deste peer.
		initiate -delete "filename" 			- Inicia a remo��o de todos os chunks de um determinado ficheiro "filename".
		initiate -reclaim 				- Inicia o protocolo de "space reclaim": apaga todos os chunks com replication degree maior do que o desejado.

O programa gera as seguintes pastas e ficheiros:

	Pasta CHUNKS: Onde guarda os chunks que recebe
	Pasta RESTORE: Onde guarda os ficheiros restaurados
	Pasta LOG:
		log.txt - A primeira linha � o espa�o m�ximo para chunks. A segunda � o espa�o ocupado.
		chunks.txt - Guarda informa��o relativa aos chunks que guardou na pasta CHUNKS: fileHash chunkNo,desiredReplicationDegree actualReplicationDegree
		backedup.txt - Guarda informa��o relativa aos ficheiros que foram backed up a partir deste peer: filename,fileHash numberChunks




