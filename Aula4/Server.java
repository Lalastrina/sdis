import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.rmi.AlreadyBoundException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

public class Server implements ServerInterface {
	//java Server <port_number>
	public static void main(String[] args) {

		String objName = args[0];
	
		try {
			Server obj = new Server();
			ServerInterface stub = (ServerInterface) UnicastRemoteObject.exportObject(obj, 0);
			System.setProperty("java.rmi.server.hostname",InetAddress.getByName(InetAddress.getLocalHost().getHostName()).getHostAddress());
			Registry registry = LocateRegistry.getRegistry();
			registry.bind(objName, stub);
			
			System.out.println("Server ready.");
			
		} catch (IOException | AlreadyBoundException e) {
			e.printStackTrace();
		}
	
	}

	@Override
	public String doThing(String request) {
		
		System.out.println("Received request: " + request);
		String[] data = request.split(" ");
		String reply = "";
		
		try {
			//build reply
			if(data[0].toUpperCase().equals("REGISTER")) {
				int result = register(data[1], data[2]);
				reply = Integer.toString(result);
			}
			else {
				reply = lookUp(data[1]);
			}
		} catch (IOException e) {
			
			e.printStackTrace();
		}
				
		System.out.print("Sent reply: " + reply);
		
		return reply;
	}
	
	public int register(String plateNumber, String owner) throws IOException {
		PrintWriter out = null;
		BufferedReader in = null;
		
		int lines = -1;
		try {
			System.out.println("WRITEEE");
		    out = new PrintWriter(new BufferedWriter(new FileWriter("db.txt", true)));
		    out.println(plateNumber);
		    out.println(owner);
		    
		} catch (IOException e) {
			e.printStackTrace();
		    System.out.println("Error occured while trying to write to DB.");
		} finally {
			out.close();
		}
		
		try {
			in = new BufferedReader(new FileReader("db.txt"));
			lines = 0;
			String s;
			while((s = in.readLine()) != null) {
				System.out.println(s);
				lines++;
			}
			
			lines /= 2;
				
		} catch (FileNotFoundException e) {
			e.printStackTrace();
            System.out.println("Could not open db file");
        } finally {
        	in.close();
        }	

		return lines;		
	}

	public String lookUp(String plateNumber) throws IOException {
		String result = "NOT_FOUND";
		BufferedReader in = null;
		boolean found = false;
		
		try {
			
            in = new BufferedReader(new FileReader("db.txt"));
            
            while(!found) {
    			
    			String line1 = in.readLine();//plate number
    			
    			if(line1 == null) 
    				break;
    			
    			String line2 = in.readLine();//owner
    			
    			if(plateNumber.equals(line1)) {
    				result = line2;
    				found = true;	
    			}
    		}
            
        } catch (FileNotFoundException e) {
        	e.printStackTrace();
            System.out.println("Could not open db file");
        } finally {
        	in.close();
        }		
		
		return result;
		
	}

}
