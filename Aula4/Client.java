import java.io.IOException;
import java.rmi.NotBoundException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class Client {

	private String remoteName;
	private String host;
	private String operation;
	private String plateNumber;
	private String owner;
	
	public Client(String[] args){
		remoteName = args[1];
		host = args[0];
		operation = args[2].toUpperCase();
		plateNumber = args[3];
		owner = "";
		
		if(operation.equals("REGISTER")){
			if(args.length != 5){
				System.out.println("java Client <host_name> <remote_namer> <oper> <opnd>");
				return;
			}
			
			owner = args[4];			
		}
	}
	
	public void run() {
		
		//create message
		String message = operation + " " + plateNumber + (owner.isEmpty()? "" : " " + owner);
		System.out.println("Message to send: " + message);
		
		try {
			
			Registry registry = LocateRegistry.getRegistry(host);
			ServerInterface server = (ServerInterface) registry.lookup(remoteName);
			String response = server.doThing(message);
					
			System.out.println(message + " : " + response);	

			
		} catch (IOException | NotBoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	
	//java Client <host_name> <remote_object> <oper> <opnd>*
	public static void main(String[] args) throws IOException {
		
		if(args.length < 4){
			System.out.println("java Client <host_name> <remote_object> <oper> <opnd>");
			return;
		}
		
		Client c = new Client(args);		
		c.run();
		
		System.out.println("Ending...");
		
	}
	
}
