import java.io.IOException;

public class Server {

	//java Server <port_number>
	public static void main(String[] args) throws NumberFormatException, IOException {
		
		if(args.length != 1) {
			System.out.println("java Server <port_number>");
			return;
		}
		
		//System.out.println(InetAddress.getLocalHost().getHostName());
		new ServerThread(new Integer(args[0])).start();
		
	}

}
