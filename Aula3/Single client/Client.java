import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class Client {

	private int port;
	private String host;
	private String operation;
	private String plateNumber;
	private String owner;
	
	public Client(String[] args){
		port = new Integer(args[1]);
		host = args[0];
		operation = args[2].toUpperCase();
		plateNumber = args[3];
		owner = "";
		
		if(operation.equals("REGISTER")){
			if(args.length != 5){
				System.out.println("java Client <host_name> <port_number> <oper> <opnd>");
				return;
			}
			
			owner = args[4];			
		}
	}
	
	public void run() {
		
		try {
			//create message
			String message = operation + " " + plateNumber + (owner.isEmpty()? "" : " " + owner);
			System.out.println("Message to send: " + message);
			
			//open socket
			Socket clientSocket = new Socket(host, port);
			
			//create input and output stream to read and write message from/to the server
			PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
			BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
			
			//send message
			out.println(message);
			
			String response = null;
			
			//get reply
			while((response = in.readLine()) != null) {
				
				System.out.println(message + " : " + response);
				response = null;
				
				out.println("QUIT");
				break;
				
			}
			
			in.close();
			out.close();
			clientSocket.close();	
			
			System.out.println("Ending...");
		} catch (IOException e) {
			System.out.println("Problem communicating with server.");
			e.printStackTrace();
		}
		
	}
	
	//java Client <host_name> <port_number> <oper> <opnd>*
	public static void main(String[] args) throws IOException {
		
		if(args.length < 4){
			System.out.println("java Client <host_name> <port_number> <oper> <opnd>");
			return;
		}
		
		Client c = new Client(args);
		c.run();
		
	}	
}
