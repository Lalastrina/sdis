import java.io.IOException;
import java.net.ServerSocket;
import java.net.SocketException;

public class Server {

	private int port;
	private ServerSocket serverSocket;
	private boolean listening;
	
	public Server(int port) throws SocketException {
		this.port = port;
		
		try {
			serverSocket = new ServerSocket(this.port);
			System.out.println("Server running...");
		} catch (IOException e) {
			System.out.println("Error opening server socket.");
			e.printStackTrace();
		}

	}
	
	public void run() {		
		
		listening = true;
		
		try {
			
			while(listening) {
				
				new ServerThread(serverSocket.accept()).start();
				
			}
			
			serverSocket.close();
			
		} catch(IOException e) {
			System.out.println("Error dealing with client request.");
			e.printStackTrace();
		} 
		
		

	}
	
	//java Server <port_number>
	public static void main(String[] args) throws NumberFormatException, IOException {
		
		if(args.length != 1) {
			System.out.println("java Server <port_number>");
			return;
		}
		
		//System.out.println(InetAddress.getLocalHost().getHostName());
		Server s = new Server(new Integer(args[0]));
		s.run();
		
	}

}