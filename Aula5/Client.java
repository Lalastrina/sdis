import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;

import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;

public class Client {

	private int port;
	private String host;
	private String operation;
	private String plateNumber;
	private String owner;
	private String[] cypherSuite;
	
	public Client(String[] args){
		port = new Integer(args[1]);
		host = args[0];
		operation = args[2].toUpperCase();
		plateNumber = args[3];
		owner = "";
		cypherSuite[0] = "";
		
		if(operation.equals("REGISTER")){
			if(args.length > 6 || args.length < 5){
				System.out.println("java Client <host_name> <port_number> <oper> <opnd> <cypher>");
				return;
			}
			
			owner = args[4];
			if(args.length == 6) 
				cypherSuite[0] = args[5];
		}
		
		if(args.length == 5)
			cypherSuite[0] = args[4];
		
	}
	
	public void run() {
		
		try {
			//create message
			String message = operation + " " + plateNumber + (owner.isEmpty()? "" : " " + owner);
			System.out.println("Message to send: " + message);
			
			//open socket
			SSLSocketFactory sslFact = (SSLSocketFactory)SSLSocketFactory.getDefault();
			SSLSocket s = (SSLSocket)sslFact.createSocket(host, port);
			
			if(!cypherSuite[0].isEmpty())
				s.setEnabledCipherSuites(cypherSuite);
			
			//create input and output stream to read and write message from/to the server
			PrintWriter out = new PrintWriter(s.getOutputStream(), true);
			BufferedReader in = new BufferedReader(new InputStreamReader(s.getInputStream()));
			
			//send message
			out.println(message);
			
			String response = null;
			
			//get reply
			while((response = in.readLine()) != null) {
				
				System.out.println(message + " : " + response);
				response = null;
				
				out.println("QUIT");
				break;
				
			}
			
			in.close();
			out.close();
			s.close();	
			
			System.out.println("Ending...");
		} catch (IOException e) {
			System.out.println("Problem communicating with server.");
			e.printStackTrace();
		}
		
	}
	
	//java SSLClient <host> <port> <oper> <opnd>* <cypher-suite>*
	public static void main(String[] args) throws IOException {
		
		if(args.length < 4){
			System.out.println("java Client <host_name> <port_number> <oper> <opnd> <cypher>");
			return;
		}
		
		Client c = new Client(args);
		c.run();
		
	}	
}
