import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.SocketException;

import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.SSLServerSocketFactory;
import javax.net.ssl.SSLSocket;

public class ServerThread extends Thread {

	private int port;
	private String[] cypherSuite;
	
	public ServerThread(int port, String cypherSuite) throws SocketException {
		super("Server09");
		this.port = port;
		this.cypherSuite[0] = cypherSuite;

	}
	
	public void run() {		

		try {
			
			SSLServerSocket s = null; 
			SSLSocket c = null;
			SSLServerSocketFactory ssf = (SSLServerSocketFactory) SSLServerSocketFactory.getDefault();
			
			s = (SSLServerSocket) ssf.createServerSocket(port);
			s.setNeedClientAuth(true);
			
			if(!cypherSuite[0].isEmpty())
				s.setEnabledCipherSuites(cypherSuite);
			
			c = (SSLSocket) s.accept();
			
			//create input and output streams to send and receive messages
			PrintWriter out = new PrintWriter(c.getOutputStream(), true);
			BufferedReader in = new BufferedReader(new InputStreamReader(c.getInputStream()));
			String inputLine = "";

			//initiate communication - client requests first
			while((inputLine = in.readLine()) != null) {
				String[] data = inputLine.split(" ");
				String result;
				System.out.println("Received request: " + inputLine);
				
				if(inputLine.toUpperCase().equals("QUIT"))					
					break;
				
				//interpret message and send reply
				if(data[0].toUpperCase().equals("REGISTER")) {
					result = Integer.toString(register(data[1], data[2]));
				}
				else {
					result = lookUp(data[1]);
				}
				
				out.println(result);
				System.out.println("Sent reply: " + result);
			}
		
			in.close();
			out.close();
			c.close();
			s.close();
			
			System.out.println("Closing...");
			
			
		} catch(IOException e) {
			System.out.println("Error dealing with client request.");
			e.printStackTrace();
		} 

	}
	
	public int register(String plateNumber, String owner) throws IOException {
		PrintWriter out = null;
		BufferedReader in = null;
		
		int lines = -1;
		try {
			System.out.println("WRITEEE");
		    out = new PrintWriter(new BufferedWriter(new FileWriter("db.txt", true)));
		    out.println(plateNumber);
		    out.println(owner);
		    
		} catch (IOException e) {
			e.printStackTrace();
		    System.out.println("Error occured while trying to write to DB.");
		} finally {
			out.close();
		}
		
		try {
			in = new BufferedReader(new FileReader("db.txt"));
			lines = 0;
			String s;
			while((s = in.readLine()) != null) {
				System.out.println(s);
				lines++;
			}
			
			lines /= 2;
				
		} catch (FileNotFoundException e) {
			e.printStackTrace();
            System.out.println("Could not open db file");
        } finally {
        	in.close();
        }	

		return lines;		
	}

	public String lookUp(String plateNumber) throws IOException {
		String result = "NOT_FOUND";
		BufferedReader in = null;
		boolean found = false;
		
		try {
			
            in = new BufferedReader(new FileReader("db.txt"));
            
            while(!found) {
    			
    			String line1 = in.readLine();//plate number
    			
    			if(line1 == null) 
    				break;
    			
    			String line2 = in.readLine();//owner
    			
    			if(plateNumber.equals(line1)) {
    				result = line2;
    				found = true;	
    			}
    		}
            
        } catch (FileNotFoundException e) {
        	e.printStackTrace();
            System.out.println("Could not open db file");
        } finally {
        	in.close();
        }		
		
		return result;
		
	}

}
