import java.io.IOException;

public class Server {

	//java SSLServer <port> <cypher-suite>*
	public static void main(String[] args) throws NumberFormatException, IOException {
		
		if(args.length < 1 || args.length > 2) {
			System.out.println("java Server <port_number> <cypher>");
			return;
		}
		
		String cypher = "";
		
		if(args.length == 2)
			cypher = args[1];
		
		//System.out.println(InetAddress.getLocalHost().getHostName());
		new ServerThread(new Integer(args[0]), cypher).start();
		
	}

}
